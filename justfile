_help:
  @just --list

# Run the playbook for a given environment
deploy environment *args: lint
  ansible-playbook \
    --inventory 'inventory.{{ environment }}.yml' \
    --vault-id '{{ environment }}@{{ env_var('ANSIBLE_VAULT_PASS_CLIENT') }}' \
    --diff \
    {{ args }} \
    'playbook.{{ environment }}.yml'

# Encrypt a named secret from stdin for a given environment
encrypt environment name:
  ansible-vault encrypt_string \
    --stdin-name '{{ name }}' \
    --vault-id '{{ environment }}@{{ env_var('ANSIBLE_VAULT_PASS_CLIENT') }}'

# Start the example server
example-up:
  vagrant up
  vagrant ssh-config --host 'example' > ~/.ssh/config.d/example

# Stop the example server
example-down:
  rm ~/.ssh/config.d/example
  vagrant destroy --force

# Check the playbook for correctness
lint:
  #!/usr/bin/env bash
  set -euo pipefail

  echo 'Linting…'
  ansible-lint --offline 2> >(grep --invert-match \
    -e 'Attempting to decrypt but no vault secrets found' \
    -e 'Skipped installing old role dependencies due to running in offline mode' >&2)

# List the facts about a server
list-facts environment hostname:
  ansible --inventory 'inventory.{{ environment }}.yml' --module-name 'ansible.builtin.setup' "{{ hostname }}"

# List the servers in a given environment
list-inventory environment:
  ansible-inventory --inventory 'inventory.{{ environment }}.yml' --list

# List the playbook tags for a given environment
list-tags environment:
  ansible-playbook --list-tags 'playbook.{{ environment }}.yml'

# Download third-party roles from Ansible Galaxy
roles:
  ansible-galaxy role install --role-file 'requirements.yml'
